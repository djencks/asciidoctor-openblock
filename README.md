# Asciidoctor openblock Extension
:version: 0.0.1

`@djencks/asciidoctor-openblock` provides an Asciidoctor.js extension.

NOTE: for more complete, better formatted README, see https://gitlab.com/djencks/asciidoctor-openblock/-/blob/master/README.adoc.

## Installation

Available soon through npm as @djencks/asciidoctor-openblock.

Currently available through a line like this in `package.json`:


    "@djencks/asciidoctor-openblock": "https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-asciidoctor-openblock-v0.0.1.tgz",


The project git repository is https://gitlab.com/djencks/asciidoctor-openblock

## Usage in asciidoctor.js

see https://gitlab.com/djencks/asciidoctor-openblock/-/blob/master/README.adoc

## Usage in Antora

see https://gitlab.com/djencks/asciidoctor-openblock/-/blob/master/README.adoc

