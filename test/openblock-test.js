/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const openblock = require('./../lib/openblock')
const { expect } = require('chai')

describe('openblock tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
  })

  ;[{
    type: 'global',
    f: (text) => {
      openblock.register(asciidoctor.Extensions)
      return asciidoctor.load(text)
    },
  },
  {
    type: 'registry',
    f: (text) => {
      const registry = openblock.register(asciidoctor.Extensions.create())
      return asciidoctor.load(text, { extension_registry: registry })
    },
  }].forEach(({ type, f }) => {
    it(`paragraph test, ${type}`, () => {
      const doc = f(`
[openblock,myrole]
This is a paragraph of one sentence.
`)
      const html = doc.convert()
      expect(html).to.equal(`<div class="openblock myrole">
<div class="content">
<div class="paragraph">
<p>This is a paragraph of one sentence.</p>
</div>
</div>
</div>`)
    })

    it(`no role test, ${type}`, () => {
      const doc = f(`
[openblock]
This is a paragraph of one sentence.
`)
      const html = doc.convert()
      expect(html).to.equal(`<div class="openblock">
<div class="content">
<div class="paragraph">
<p>This is a paragraph of one sentence.</p>
</div>
</div>
</div>`)
    })

    it(`multiple roles test, ${type}`, () => {
      const doc = f(`
[openblock,role1 role2]
This is a paragraph of one sentence.
`)
      const html = doc.convert()
      expect(html).to.equal(`<div class="openblock role1 role2">
<div class="content">
<div class="paragraph">
<p>This is a paragraph of one sentence.</p>
</div>
</div>
</div>`)
    })

    it(`block test, ${type}`, () => {
      const doc = f(`= Title
      
== Section 0

A previous paragraph.

[openblock,myrole]
----
[discrete]
== Section 1

This is a paragraph of one sentence.

[discrete]
== Section 2

This is a paragraph of two sentences.
The second sentence.
----
`)
      const html = doc.convert()
      expect(html).to.equal(`<div class="sect1">
<h2 id="_section_0">Section 0</h2>
<div class="sectionbody">
<div class="paragraph">
<p>A previous paragraph.</p>
</div>
<div class="openblock myrole">
<div class="content">
<h2 id="_section_1" class="discrete">Section 1</h2>
<div class="paragraph">
<p>This is a paragraph of one sentence.</p>
</div>
<h2 id="_section_2" class="discrete">Section 2</h2>
<div class="paragraph">
<p>This is a paragraph of two sentences.
The second sentence.</p>
</div>
</div>
</div>
</div>
</div>`)
    })

    it(`recursive test, ${type}`, () => {
      const doc = f(`
[openblock,myrole]
----
Prelude

[openblock,role2]
------
The fabulous nested content.
------

Postlude
----
`)
      const html = doc.convert()
      expect(html).to.equal(`<div class="openblock myrole">
<div class="content">
<div class="paragraph">
<p>Prelude</p>
</div>
<div class="openblock role2">
<div class="content">
<div class="paragraph">
<p>The fabulous nested content.</p>
</div>
</div>
</div>
<div class="paragraph">
<p>Postlude</p>
</div>
</div>
</div>`)
    })

    it(`nested in an open block test, ${type}`, () => {
      const doc = f(`
[.outer-role]
--
Preceding.

[openblock,myrole]
This is a paragraph of one sentence.

Following.
--
`)
      const html = doc.convert()
      expect(html).to.equal(`<div class="openblock outer-role">
<div class="content">
<div class="paragraph">
<p>Preceding.</p>
</div>
<div class="openblock myrole">
<div class="content">
<div class="paragraph">
<p>This is a paragraph of one sentence.</p>
</div>
</div>
</div>
<div class="paragraph">
<p>Following.</p>
</div>
</div>
</div>`)
    })

    it(`nesting an open block test, ${type}`, () => {
      const doc = f(`
[openblock, outer-role]
----
Preceding.

[.myrole]
--
This is a paragraph of one sentence.
--

Following.
----
`)
      const html = doc.convert()
      expect(html).to.equal(`<div class="openblock outer-role">
<div class="content">
<div class="paragraph">
<p>Preceding.</p>
</div>
<div class="openblock myrole">
<div class="content">
<div class="paragraph">
<p>This is a paragraph of one sentence.</p>
</div>
</div>
</div>
<div class="paragraph">
<p>Following.</p>
</div>
</div>
</div>`)
    })
  })
})
